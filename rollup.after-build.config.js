import dts from 'rollup-plugin-dts'

const fs = require('fs')
const path = require('path')

const searchRecursive = function (dir, pattern) {
  let results = []
  fs.readdirSync(dir).forEach(function (dirInner) {
    dirInner = path.resolve(dir, dirInner)
    const stat = fs.statSync(dirInner)
    if (stat.isDirectory()) {
      results = results.concat(searchRecursive(dirInner, pattern))
    }
    if (stat.isFile() && dirInner.endsWith(pattern)) {
      results.push(dirInner)
    }
  })

  return results
}

const getDTS = () => {
  const items = []
  const files = searchRecursive('./dist', '.d.ts')
  files.forEach((item) => {
    items.push({
      input: item,
      output: [{ file: item, format: 'esm' }],
      plugins: [dts()],
      external: [/\.(css|less|scss)$/],
    })
  })

  return items
}

export default getDTS()
