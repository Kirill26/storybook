import React from 'react'
import { ReactElement } from 'react'

interface SvgProps {
  fill?: string
}

export const Minus = ({ fill }: SvgProps): ReactElement => (
  <svg
    fill="none"
    height="24"
    viewBox="0 0 24 24"
    width="24"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M19 13H5V11H19V13Z" fill={fill || '#13294B'} />
  </svg>
)
