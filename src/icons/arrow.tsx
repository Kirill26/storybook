import React from 'react'
import { ReactElement } from 'react'

interface SvgProps {
  fill?: string
}

export const Arrow = ({ fill }: SvgProps): ReactElement => (
  <svg fill="none" height="24" width="24" xmlns="http://www.w3.org/2000/svg">
    <path
      d="m10.004 6-1.41 1.41 4.58 4.59-4.58 4.59 1.41 1.41 6-6-6-6Z"
      fill={fill || '#13294B'}
    ></path>
  </svg>
)
