import type { Meta } from '@storybook/react'
import React from 'react'

import * as svg from './index'

export const Svg = () => (
  <ul
    style={{
      display: 'flex',
      flexWrap: 'wrap',
      listStyle: 'none',
      padding: '0',
    }}
  >
    {Object.entries(svg).map(([title, Icon]) => (
      <li
        key={title}
        style={{
          margin: '10px',
          width: '100%',
          maxWidth: '100px',
          textAlign: 'center',
          boxShadow: '4px 4px 8px 0 rgb(34 60 80 / 20%)',
          padding: '15px',
          borderRadius: '4px',
        }}
      >
        <Icon />
        <p style={{ margin: '10px 0 0 0' }}>{title}</p>
      </li>
    ))}
  </ul>
)

export default {
  title: 'Design/Icons',
} as Meta
