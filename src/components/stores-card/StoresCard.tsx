import React, { useCallback, useRef, useState } from 'react'
import clsx from 'clsx'
import stores from './storesCard.module.scss'
import { useClickOutside } from '../../common/hooks/useClickOutside'

interface StoresCardProps {
  image?: string
  link?: string
  location?: string
  name?: string
  statuses?: any
}

const StoresCard = ({
  image,
  link,
  location,
  name,
  statuses,
}: StoresCardProps): JSX.Element => {
  const elRef = useRef()
  const [openSellerBlock, setSellerBlock] = useState(false)

  const handleClick = useCallback(() => {
    setSellerBlock(!openSellerBlock)
  }, [openSellerBlock])

  useClickOutside(elRef, () => setSellerBlock(false))

  return (
    <li
      className={stores.element}
      itemScope
      itemType="https://schema.org/Organization"
    >
      <a href={link} className={clsx(stores.link)} itemProp="url">
        <img
          alt={name}
          className={stores.img}
          itemProp="logo"
          loading="lazy"
          src={image}
        />
      </a>
      {location && (
        <p className={stores.location} itemProp="location">
          {location}
        </p>
      )}
      {name && (
        <a className={stores.name} href={link} itemProp="url">
          <span itemProp="legalName">{name}</span>
        </a>
      )}
      {statuses?.length > 0 && (
        <ul className={stores.sellers}>
          {statuses.slice(0, 1).map((item: string, index: number) => (
            <li className={stores.sellers__element} key={index}>
              {item}
            </li>
          ))}
          <li>
            {statuses.length > 1 && (
              // @ts-ignore
              <div ref={elRef} style={{ position: 'relative' }}>
                {statuses?.length > 1 && openSellerBlock && (
                  <ul
                    className={clsx(stores.sellers__popup, {
                      [stores.display]: openSellerBlock,
                    })}
                  >
                    {statuses.slice(1).map((item: string, index: number) => (
                      <li className={stores.sellers__element} key={index}>
                        {item}
                      </li>
                    ))}
                  </ul>
                )}
                <button
                  className={clsx(stores.sellers__element, stores.sellers__btn)}
                  onClick={handleClick}
                  type="button"
                >
                  ...
                </button>
              </div>
            )}
          </li>
        </ul>
      )}
    </li>
  )
}
StoresCard.defaultProps = {}
export default StoresCard
