import React, { ReactNode } from 'react'
import clsx from 'clsx'
import style from './card.module.scss'

interface CardProps {
  border?: boolean
  children: ReactNode
  className?: string
  component: 'article' | 'li' | 'a' | 'div'
  href?: string
  itemScope?: boolean
  itemType?: string
  shadow?: boolean
}

const Card = ({
  border,
  children,
  className,
  component,
  href,
  itemScope,
  itemType,
  shadow,
}: CardProps): JSX.Element => {
  const Block = component as keyof JSX.IntrinsicElements

  return (
    <Block
      href={href}
      itemScope={itemScope}
      className={clsx(
        style.card,
        { [style.cardShadow]: shadow },
        { [style.cardBorder]: border },
        className,
      )}
      itemType={itemType}
    >
      {children}
    </Block>
  )
}

Card.defaultProps = {
  component: 'div',
}

export default Card
