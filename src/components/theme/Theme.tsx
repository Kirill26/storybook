import React from 'react'

interface ThemeProviderProps {
  children: React.ReactNode
  theme?: string
}

export const ThemeContext = React.createContext('')

const ThemeProvider = ({
  children,
  theme,
}: ThemeProviderProps): JSX.Element => {
  return (
    <>
      <ThemeContext.Provider value={theme ? theme : 'b2bTheme'}>
        {children}
      </ThemeContext.Provider>
    </>
  )
}
export default ThemeProvider
