import React from 'react'
import clsx from 'clsx'
import card from './productCard.module.scss'

interface ProductCardProps {
  className?: string
  currencySymbol?: string
  description?: string
  findOutPrice?: {
    title: string
    disabled: boolean
    onClick: React.MouseEventHandler<HTMLButtonElement>
  }
  locations?: string[] | null
  minOrderQuantity?: number
  minOrderUnitType?: string | null
  name?: string
  prefix?: string
  price?: string
  priceUnit?: string
  promo?: string
  storeAlias?: string | null
  storeName?: string
}

const ProductCard = ({
  currencySymbol,
  description,
  findOutPrice,
  locations,
  minOrderQuantity,
  minOrderUnitType,
  name,
  prefix,
  price,
  priceUnit,
  promo,
  storeAlias,
  storeName,
}: ProductCardProps): JSX.Element => {
  return (
    <article
      className={clsx(card.article, { [card.isPromo]: promo })}
      style={{ width: '187px' }}
    >
      <a className={card.link}>
        <img
          className={card.box__img}
          src="https://img.b2b.trade/4f8dc554-bdde-4001-9bce-488c707a4e47/-/smart_resize/500x500/-/quality/smart/-/format/webp/"
        />
      </a>
      {name && (
        <a className={card.name}>
          <span>{name}</span>
        </a>
      )}
      <div className={card.descriptionHide} itemProp="description">
        {description}
      </div>
      {locations && (
        <p className={card.location}>
          <span>{locations[0]}</span>
        </p>
      )}
      {storeName && storeAlias && (
        <a href={`/store/${storeAlias}`} className={card.store}>
          <span>{storeName}</span>
        </a>
      )}
      {minOrderQuantity !== 0 && minOrderUnitType && (
        <p className={card.count}>
          {prefix} {` ${minOrderQuantity} ${minOrderUnitType}`}
        </p>
      )}

      <div className={card.actions}>
        {price ? (
          <div className={card.price}>
            <p>{price}</p>
            <span>
              <span>
                <span className={card.priceCurrencyHide}></span>
                <span>{currencySymbol}</span>
              </span>
              / {priceUnit}
            </span>
          </div>
        ) : (
          <button
            disabled={findOutPrice?.disabled}
            onClick={findOutPrice?.onClick}
            className={card.price_btn}
          >
            {findOutPrice?.title}
          </button>
        )}
      </div>
      {promo && <p className={card.isPromoBlock}>{promo}</p>}
    </article>
  )
}

ProductCard.defaultProps = {}
export default ProductCard
