// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React, { ReactNode, useState } from 'react'
import { useScrollBlock } from '../../common/hooks/useScrollBlock'
import Button from '../button/Button'
import Modal from './Modal'

interface ModalExampleProps {
  children: ReactNode
  label: string
  fullScreen?: boolean
  overlay?: boolean
  position: 'top' | 'center' | 'bottom'
}

const ModalExample = ({
  fullScreen,
  label,
  overlay,
  position,
  children,
}: ModalExampleProps): JSX.Element => {
  const [isOpen, setIsOpen] = useState(false)
  const [blockScroll, allowScroll] = useScrollBlock()

  const openModal = (): void => {
    setIsOpen(!isOpen)
    blockScroll()
  }
  const closeModal = (): void => {
    setIsOpen(!isOpen)
    allowScroll()
  }

  return (
    <>
      <Button
        color="secondary"
        onClick={openModal}
        size="medium"
        variant="contained"
      >
        {label}
      </Button>
      <Modal
        fullScreen={fullScreen}
        open={isOpen}
        overlay={overlay}
        position={position}
      >
        {children}
        <Button
          color="secondary"
          onClick={closeModal}
          size="small"
          variant="outlined"
        >
          close
        </Button>
      </Modal>
    </>
  )
}

export default ModalExample
