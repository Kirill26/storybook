import React, { ReactNode } from 'react'
import clsx from 'clsx'
import m from './modal.module.scss'
import { Portal } from '../../common/portal'

interface ModalProps {
  className?: string
  children: ReactNode
  fullScreen?: boolean
  overlay?: boolean
  position: 'top' | 'center' | 'bottom'
  open: boolean
}

const Modal = ({
  children,
  className,
  fullScreen,
  overlay,
  position,
  open,
}: ModalProps): JSX.Element => {
  function ucFirst(str: string | undefined) {
    if (!str) return str

    return str[0].toUpperCase() + str.slice(1)
  }
  const modalPosition = ucFirst(position)

  return (
    <>
      {open && (
        <Portal
          className={clsx(
            m.modal,
            { [m[`modal${modalPosition}`]]: modalPosition },
            {
              [m.overlay]: overlay,
            },
          )}
        >
          <div
            className={clsx(
              { [m.fullScreen]: fullScreen },
              m.modalBlock,
              className,
            )}
          >
            {children}
          </div>
        </Portal>
      )}
    </>
  )
}

Modal.defaultProps = {
  fullScreen: false,
  overlay: true,
}

export default Modal
