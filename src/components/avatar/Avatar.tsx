// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React from 'react'
import clsx from 'clsx'
import style from './avatar.module.scss'

interface AvatarProps {
  className?: string
  label: string
  image?: string
  link: string
}

const Avatar = ({
  className,
  label,
  link,
  image,
}: AvatarProps): JSX.Element => (
  <a className={clsx(className, style.avatar)} href={link}>
    {/* eslint-disable-next-line @next/next/no-img-element */}
    {image ? <img alt={label} src={image} title={label} /> : label}
  </a>
)

Avatar.defaultProps = {
  className: null,
  image: null,
}

export default Avatar
