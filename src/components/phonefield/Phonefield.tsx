// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React, { ReactNode } from 'react'
import PhoneInput from 'react-phone-input-2'
// @ts-ignore
import ru from 'react-phone-input-2/lang/ru.json'
import 'react-phone-input-2/lib/material.css'
import clsx from 'clsx'
import { Phone } from '../../icons'
import { ThemeContext } from '../theme/Theme'
import p from './phonefield.module.scss'

interface PhonefieldProps {
  className?: string
  country: string
  disabled?: boolean
  id?: string
  isValid?: any
  errorText?: string
  helperText?: string | ReactNode
  label: string
  name?: string
  onChange?: any
  onlyCountries?: any
  placeholder?: string
  regions?: string
  required?: boolean
  preferredCountries: any
  prompt?: string
  value: string
}

const Phonefield = ({
  className,
  country,
  disabled,
  id,
  isValid,
  errorText,
  helperText,
  label,
  name,
  onChange,
  onlyCountries,
  placeholder,
  preferredCountries,
  regions,
  required,
  value,
}: PhonefieldProps): JSX.Element => {
  const theme = React.useContext(ThemeContext)

  return (
    <div
      className={clsx(
        theme,
        p.container,
        { [p.disabled]: disabled },
        { [p.error]: errorText },
        { [p.noLabel]: !label },
        className,
      )}
    >
      {label && <p className={p.label}>{label}</p>}
      <PhoneInput
        country={country}
        dropdownClass={p.dropdown}
        inputClass={p.input}
        inputProps={{
          id: id,
          name: name,
          disabled: disabled,
          required: required,
        }}
        isValid={isValid}
        localization={ru}
        onChange={onChange}
        onlyCountries={onlyCountries}
        placeholder={placeholder}
        preferredCountries={preferredCountries}
        regions={regions}
        value={value}
      />
      {errorText && <p className={clsx(p.prompt, p.errorText)}>{errorText}</p>}
      {helperText && (
        <p className={clsx(p.prompt, p.helperText)}>{helperText}</p>
      )}
      <div className={p.icon}>
        <Phone fill="#717F93" />
      </div>
    </div>
  )
}
Phonefield.defaultProps = {
  disabled: false,
  placeholder: null,
  isValid: true,
}

export default Phonefield
