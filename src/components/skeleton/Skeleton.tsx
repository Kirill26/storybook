// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React from 'react'
import clsx from 'clsx'
import styles from './skeleton.module.scss'

interface SkeletonProps {
  className?: string
  height?: string
  margin?: string
  type: 'circle' | 'box'
  width?: string
}

const Skeleton = ({
  className,
  height,
  margin,
  type,
  width,
}: SkeletonProps): JSX.Element => {
  const skeletonType = type[0]?.toUpperCase() + type.slice(1)

  return (
    <div
      className={clsx(
        styles.skeleton,
        className,
        styles[`skeleton${skeletonType}`],
      )}
      style={{ width: `${width}`, height: `${height}`, margin: `${margin}` }}
    />
  )
}

Skeleton.defaultProps = {
  className: null,
  height: null,
  margin: null,
  width: null,
}

export default Skeleton
