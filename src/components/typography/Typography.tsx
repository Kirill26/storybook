// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React, { ReactNode } from 'react'
import clsx from 'clsx'
import t from './typography.module.scss'

interface HeadingProps {
  children: ReactNode
  className?: string
  tag: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p'
}

const Typography = ({
  children,
  className,
  tag,
}: HeadingProps): JSX.Element => {
  const Title = tag as keyof JSX.IntrinsicElements

  return <Title className={clsx(t[`${tag}`], className)}>{children}</Title>
}

Typography.defaultProps = {
  className: null,
}

export default Typography
