// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React, {
  ChangeEventHandler,
  InputHTMLAttributes,
  KeyboardEventHandler,
  ReactNode,
  useRef,
} from 'react'
import clsx from 'clsx'
// import Button from '../button/Button'
import { /*Close,*/ Eye, Mail, Search } from '../../icons'
import { ThemeContext } from '../theme/Theme'
import t from './textfield.module.scss'

interface TextfieldProps {
  className?: string
  disabled?: boolean
  id?: string
  disableReset?: boolean
  label?: string
  name?: string
  onChangeInput?: ChangeEventHandler<HTMLInputElement>
  onChangeTextArea?: ChangeEventHandler<HTMLTextAreaElement>
  onKeyDown?: KeyboardEventHandler<HTMLInputElement>
  placeholder?: string
  prefix?: ReactNode
  required?: boolean
  rows?: number
  suffix?: ReactNode
  textarea?: boolean
  type?: 'text' | 'number' | 'search' | 'password' | 'email' | any
  value?: string | number
  errorText?: string | ReactNode
  helperText?: string
  inputProps?: InputHTMLAttributes<HTMLInputElement>
}

const Textfield = ({
  className,
  disabled,
  id,
  inputProps,
  // disableReset,
  label,
  name,
  onChangeInput,
  onChangeTextArea,
  onKeyDown,
  placeholder,
  prefix,
  required,
  rows,
  suffix,
  textarea,
  type,
  value,
  errorText,
  helperText,
}: TextfieldProps): JSX.Element => {
  const ref = useRef(null)
  const theme = React.useContext(ThemeContext)

  /*  const resetClick = (): void => {
    ref.current.value = ''
  }*/

  return (
    <label
      className={clsx(
        theme,
        t.textfield,
        className,
        { [t.disabled]: disabled },
        { [t.noLabel]: !label },
        { [t.fullWidth]: type === 'text' || 'number' },
        { [t.nowrap]: prefix || suffix },
        { [t.error]: errorText },
      )}
      htmlFor={id}
    >
      {label && <p className={t.label}>{label}</p>}
      {prefix && <div className={t.prefix}>{prefix}</div>}
      {!textarea ? (
        <input
          autoComplete="off"
          disabled={disabled}
          id={id}
          name={name}
          onChange={onChangeInput}
          onKeyDown={onKeyDown}
          placeholder={placeholder}
          ref={ref}
          required={required}
          type={type}
          value={value}
          {...inputProps}
        />
      ) : (
        <textarea
          disabled={disabled}
          id={id}
          name={name}
          onChange={onChangeTextArea}
          placeholder={label}
          ref={ref}
          required={required}
          rows={rows}
          value={value}
        />
      )}
      {suffix && <div className={t.suffix}>{suffix}</div>}

      <div className={t.iconBlock}>
        {/*{!disableReset && (
          <Button className={t.closeButton} onClick={resetClick} onlyIcon>
            <Close />
          </Button>
        )}*/}
        {type === 'search' && <Search fill="#717F93" />}
        {type === 'password' && <Eye fill="#717F93" />}
        {type === 'email' && <Mail fill="#717F93" />}
      </div>
      {errorText && (
        <p className={clsx(t.promptText, t.errorText)}>{errorText}</p>
      )}
      {helperText && (
        <p className={clsx(t.promptText, t.helperText)}>{helperText}</p>
      )}
    </label>
  )
}
Textfield.defaultProps = {
  className: null,
  prompt: null,
  textarea: false,
}

export default Textfield
