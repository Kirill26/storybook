import React, { ReactNode } from 'react'
import { ThemeContext } from '../theme/Theme'
import clsx from 'clsx'
import b from './Button.module.scss'

interface ButtonProps {
  children: ReactNode
  className?: string
  color: 'primary' | 'secondary'
  disabled?: boolean
  onClick?: React.MouseEventHandler<HTMLButtonElement>
  onlyIcon?: boolean
  size: 'large' | 'medium' | 'small'
  type?: 'button' | 'reset' | 'submit'
  variant: 'contained' | 'outlined' | 'text' | 'simple'
}

const ThemedButton = ({
  children,
  // className,
  // color,
  disabled,
  onClick,
  // onlyIcon,
  // size,
  type,
  variant,
}: ButtonProps): JSX.Element => {
  function ucFirst(str: string | undefined) {
    if (!str) return str

    return str[0].toUpperCase() + str.slice(1)
  }
  // const buttonColor = ucFirst(color)
  // const buttonSize = ucFirst(size)
  // const buttonVariant = ucFirst(variant)
  const value = React.useContext(ThemeContext)

  return (
    <>
      {variant !== 'simple' ? (
        <button
          // className="button"
          className={clsx(value, b.button)}
          disabled={disabled}
          onClick={onClick}
          type={type}
        >
          {children}
        </button>
      ) : (
        <button
          // className={clsx(b.button, className)}
          disabled={disabled}
          onClick={onClick}
          type={type}
        >
          {children}
        </button>
      )}
    </>
  )
}
export default ThemedButton
