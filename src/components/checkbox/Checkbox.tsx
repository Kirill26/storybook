// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React, { ChangeEventHandler } from 'react'
import clsx from 'clsx'
import checkbox from './checkbox.module.scss'
import { ThemeContext } from '../theme/Theme'

interface CheckboxProps {
  checked: boolean
  className?: string
  disabled?: boolean
  id?: string
  label?: string
  name: string
  onChange?: ChangeEventHandler<HTMLInputElement>
  required?: boolean
  size: 'medium' | 'small'
  style?: object
}

const Checkbox = ({
  checked,
  className,
  disabled,
  id,
  label,
  name,
  onChange,
  required,
  size,
  style,
}: CheckboxProps): JSX.Element => {
  const checkboxSize = size[0]?.toUpperCase() + size.slice(1)
  const theme = React.useContext(ThemeContext)

  return (
    <>
      <label
        className={clsx(
          theme,
          className,
          checkbox.label,
          checkbox[`label${checkboxSize}`],
          {
            [checkbox.disabled]: disabled,
          },
        )}
        htmlFor={id}
        style={style}
      >
        {label}
        <input
          checked={checked}
          disabled={disabled}
          id={id}
          name={name}
          onChange={onChange}
          required={required}
          type="checkbox"
        />
        <span className={checkbox.checkmark} />
      </label>
    </>
  )
}

Checkbox.defaultProps = {
  className: null,
  disabled: false,
  id: null,
  label: null,
  onChange: null,
  ref: null,
  required: false,
}

export default Checkbox
