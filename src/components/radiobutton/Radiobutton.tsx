// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React, { ChangeEventHandler } from 'react'
import clsx from 'clsx'
import radiobutton from './radiobutton.module.scss'
import { ThemeContext } from '../theme/Theme'

interface RadiobuttonProps {
  checked: boolean
  className?: string
  disabled: boolean
  id: string
  label?: string
  name: string
  onChange: ChangeEventHandler<HTMLInputElement>
  required: boolean
  ref?: string
  size: 'medium' | 'small'
}

const Radiobutton = ({
  checked,
  className,
  disabled,
  id,
  label,
  name,
  onChange,
  ref,
  required,
  size,
}: RadiobuttonProps): JSX.Element => {
  const radioSize = size[0]?.toUpperCase() + size.slice(1)
  const theme = React.useContext(ThemeContext)

  return (
    <>
      <label
        className={clsx(
          theme,
          className,
          radiobutton.label,
          radiobutton[`label${radioSize}`],
          {
            [radiobutton.disabled]: disabled,
          },
        )}
        htmlFor={id}
        ref={ref}
      >
        <p>{label}</p>
        <input
          checked={checked}
          disabled={disabled}
          id={id}
          name={name}
          onChange={onChange}
          required={required}
          type="radio"
        />
        <span className={radiobutton.checkmark} />
      </label>
    </>
  )
}

Radiobutton.defaultProps = {
  className: null,
  label: null,
  ref: null,
}

export default Radiobutton
