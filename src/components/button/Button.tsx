import React, { ReactNode } from 'react'
import clsx from 'clsx'
import b from './Button.module.scss'
import { ThemeContext } from '../theme/Theme'

interface ButtonProps {
  children: ReactNode
  className?: string
  color: 'primary' | 'secondary'
  disabled?: boolean
  onClick?: React.MouseEventHandler<HTMLButtonElement>
  onlyIcon?: boolean
  size: 'large' | 'medium' | 'small'
  type?: 'button' | 'reset' | 'submit'
  variant: 'contained' | 'outlined' | 'text' | 'simple'
}

const Button = ({
  children,
  className,
  color,
  disabled,
  onClick,
  onlyIcon,
  size,
  type,
  variant,
}: ButtonProps): JSX.Element => {
  function ucFirst(str: string | undefined) {
    if (!str) return str

    return str[0].toUpperCase() + str.slice(1)
  }
  const buttonSize = ucFirst(size)
  const buttonColor = ucFirst(color)
  const buttonVariant = ucFirst(variant)

  const theme = React.useContext(ThemeContext)

  return (
    <>
      {variant !== 'simple' ? (
        <button
          className={clsx(
            theme,
            b.button,
            { [b[`button${buttonSize}`]]: size },
            { [b[`button${buttonVariant}${buttonColor}`]]: variant },
            { [b.buttonIcon]: onlyIcon },
            className,
          )}
          disabled={disabled}
          onClick={onClick}
          type={type}
        >
          {children}
        </button>
      ) : (
        <button
          className={clsx(b.button, className)}
          disabled={disabled}
          onClick={onClick}
          type={type}
        >
          {children}
        </button>
      )}
    </>
  )
}

Button.defaultProps = {
  className: null,
  color: 'primary',
  disabled: false,
  onClick: null,
  size: 'large',
  type: 'button',
  variant: 'contained',
}

export default Button
