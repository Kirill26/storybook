// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React, { ChangeEventHandler } from 'react'
import clsx from 'clsx'
import s from './switchbox.module.scss'
import { ThemeContext } from '../theme/Theme'

interface SwitchboxProps {
  checked: boolean
  className?: string
  id: string
  label?: string
  name: string
  onChange: ChangeEventHandler<HTMLInputElement>
  required: boolean
  size: 'medium' | 'small'
}

const Switchbox = ({
  checked,
  className,
  id,
  label,
  name,
  onChange,
  required,
  size,
}: SwitchboxProps): JSX.Element => {
  const switchSize = size[0]?.toUpperCase() + size.slice(1)
  const theme = React.useContext(ThemeContext)

  return (
    <>
      <label className={clsx(theme, className, s.label)}>
        <div className={clsx(s.switch, s[`switch${switchSize}`])}>
          <input
            checked={checked}
            id={id}
            name={name}
            onChange={onChange}
            required={required}
            type="checkbox"
          />
          <span className={s.switchBlock}>
            <span className={s.on} />
            <span className={s.off} />
          </span>
        </div>
        {label && <p className={s.switchText}>{label}</p>}
      </label>
    </>
  )
}

Switchbox.defaultProps = {
  className: null,
  label: null,
}

export default Switchbox
