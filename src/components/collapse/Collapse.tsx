// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React, { ReactNode, useState, useRef } from 'react'
import clsx from 'clsx'
import clps from './collapse.module.scss'
import Typography from '../typography/Typography'

interface CollapseProps {
  accordion?: boolean
  children: ReactNode
  className?: string
  contentPosition?: 'top' | 'bottom'
  label: ReactNode
  collapsedLabel?: string
}

const Collapse = ({
  accordion,
  children,
  className,
  contentPosition,
  collapsedLabel,
  label,
}: CollapseProps): JSX.Element => {
  const [open, setOPen] = useState(false)
  const toggle = (): void => {
    setOPen(!open)
  }
  const contentRef = useRef<any>()

  function ucFirst(str: string | undefined) {
    if (!str) return str

    return str[0].toUpperCase() + str.slice(1)
  }
  const collapsePosition = ucFirst(contentPosition)

  return (
    <div
      className={clsx(
        clps.collapse,
        { [clps.collapseOpen]: open },
        { [clps.accordion]: accordion },
      )}
    >
      <button
        className={clsx(clps[`button${collapsePosition}`], clps.collapseButton)}
        onClick={toggle}
      >
        {open && collapsedLabel ? collapsedLabel : label}
      </button>
      <div
        className={clsx(clps.content, { [clps.contentOpen]: open }, className)}
        ref={contentRef}
        style={
          open
            ? { height: `${contentRef.current.scrollHeight}px` }
            : { height: '0px' }
        }
      >
        {accordion ? <Typography tag="p">{children}</Typography> : children}
      </div>
    </div>
  )
}

Collapse.defaultProps = {
  accordion: false,
  className: null,
  collapsedLabel: null,
  contentPosition: 'bottom',
  icon: true,
}

export default Collapse
