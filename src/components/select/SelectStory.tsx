// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React from 'react'
import Select from './Select'

const options = ['chocolate', 'strawberry', 'vanilla']

const SelectStory = (): JSX.Element => {
  return (
    <div style={{ margin: '10px', height: '230px' }}>
      <Select
        label={'Label'}
        options={options?.map((item) => ({
          label: item,
          value: item,
        }))}
        placeholder={'Placeholder'}
      />
    </div>
  )
}

export default SelectStory
