// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React, { ChangeEventHandler } from 'react'
import clsx from 'clsx'
import SelectComponent, {
  components,
  DropdownIndicatorProps,
  GroupBase,
} from 'react-select'
import { Home } from '../../icons'
import style from './select.module.scss'

interface SelectProps {
  label?: string
  onChange?: ChangeEventHandler<HTMLInputElement> | any
  options: any
  placeholder?: string
  value?: string
}

const resetStyles = {
  control: (base: any, state: { isFocused: any }) => ({
    ...base,
    borderColor: state.isFocused ? null : null,
    boxShadow: state.isFocused ? null : null,
    '&:hover': {
      borderColor: state.isFocused ? null : null,
    },
  }),
}

const DropdownIndicator = (
  props: JSX.IntrinsicAttributes &
    DropdownIndicatorProps<unknown, boolean, GroupBase<unknown>>,
) => {
  return (
    <components.DropdownIndicator {...props}>
      <Home fill={'13294B'} />
    </components.DropdownIndicator>
  )
}

const Select = ({
  label,
  onChange,
  options,
  placeholder,
  value,
}: SelectProps): JSX.Element => {
  return (
    <div className={style.container}>
      <SelectComponent
        className={style.select}
        components={{ DropdownIndicator }}
        onChange={onChange}
        options={options}
        placeholder={placeholder || ''}
        styles={resetStyles}
        value={value}
      />
      <p className={style.label}>{label}</p>
    </div>
  )
}

export default Select
