// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React from 'react'
import clsx from 'clsx'
import color from './colors.module.scss'

const b2bTheme = [
  {
    name: 'primary',
    color: '#FF2402',
  },
  {
    name: 'primaryLight',
    color: '#FF7C67',
  },
  {
    name: 'primaryLighter',
    color: '#FFE4E0',
  },
  {
    name: 'primaryDark',
    color: '#CF1D02',
  },
  {
    name: 'secondary',
    color: '#13294B',
  },
  {
    name: 'secondaryLight',
    color: '#717F93',
  },
  {
    name: 'secondaryLighter',
    color: '#E2E5E9',
  },
  {
    name: 'secondaryLightest',
    color: '#F6F7F8',
  },
  {
    name: 'secondaryDark',
    color: '#0A182F',
  },
]
const b2bGreen = [
  {
    name: 'primary',
    color: '#22D86C',
  },
  {
    name: 'primaryLight',
    color: '#7AE8A7',
  },
  {
    name: 'primaryLighter',
    color: '#E4FAED',
  },
  {
    name: 'primaryLightest',
    color: '#7AE8A7',
  },
  {
    name: 'primaryDark',
    color: '#0C8E41',
  },
  {
    name: 'secondary',
    color: '#003333',
  },
  {
    name: 'secondaryLight',
    color: '#668585',
  },
  {
    name: 'secondaryLighter',
    color: '#E0E6E6',
  },
  {
    name: 'secondaryLightest',
    color: '#A3B5B5',
  },
  {
    name: 'secondaryDark',
    color: '#011C1C',
  },
]

const Color = (): JSX.Element => (
  <div className={color.container}>
    <ul className={color.list}>
      <p className={color.title}>b2bTheme Colours</p>
      {b2bTheme.map((item, index) => (
        <li key={index}>
          <div
            className={color.listBlock}
            style={{ backgroundColor: item.color }}
          />
          <p>
            <span>{item.name}</span>
            {item.color}
          </p>
        </li>
      ))}
    </ul>
    <div className={color.separator} />
    <ul className={color.list}>
      <p className={color.title}>b2bGreen Colours</p>
      {b2bGreen.map((item, index) => (
        <li key={index}>
          <div
            className={color.listBlock}
            style={{ backgroundColor: item.color }}
          />
          <p>
            <span>{item.name}</span>
            {item.color}
          </p>
        </li>
      ))}
    </ul>
  </div>
)

export default Color
