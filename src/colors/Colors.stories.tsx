// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import Colors from './Colors'

// @ts-ignore
const Template: ComponentStory<typeof Colors> = (args) => <Colors {...args} />

export const colorsList = Template.bind({})
colorsList.args = {}

export default {
  title: 'Design/Colors',
  component: Colors,
  parameters: {
    docs: {
      page: null,
    },
  },
} as ComponentMeta<typeof Colors>
