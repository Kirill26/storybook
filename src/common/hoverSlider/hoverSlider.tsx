// eslint-disable-next-line @typescript-eslint/no-use-before-define
import React, { FC, useEffect, useRef, useState } from 'react'
import styles from './hoverSlider.module.scss'
import { UploadcareImage } from '../uploadImage'

export interface ImageModel {
  fileId?: string
  position?: number
  uploadcareId?: string | null
  urlModifiers?: string | null
  uploadcareUrlPath?: string | null
}

interface HoverSliderProps {
  alt?: string
  images: ImageModel[]
  itemProp?: string
}

const HoverSlider: FC<HoverSliderProps> = ({
  alt = '',
  images,
  itemProp,
}: HoverSliderProps) => {
  const [userInteracted, setUserInteracted] = useState(false)
  const activeImagePosition = useRef(null)
  const itemsRef = useRef([])
  const indicatorsRef = useRef([])

  useEffect(() => {
    itemsRef.current[0].classList.add(styles.item__active)
    indicatorsRef.current[0].classList.add(styles.indicators_item__active)
  }, [])

  return (
    <div
      className={styles.root}
      onMouseMove={(event): void => {
        if (!userInteracted) {
          setUserInteracted(true)
        }
        const currentTargetRect = event.currentTarget.getBoundingClientRect()
        const eventOffsetX = event.pageX - currentTargetRect.left
        const itemsBreakpoints = []
        const itemBreakpoint = currentTargetRect.width / images.length
        for (let i = 1; i <= images.length; i++) {
          itemsBreakpoints.push(itemBreakpoint * i)
        }
        for (let i = 0; i < itemsBreakpoints.length; i++) {
          if (eventOffsetX <= itemsBreakpoints[i]) {
            if (activeImagePosition.current !== i) {
              activeImagePosition.current = i
              for (let i2 = 0; i2 < itemsRef.current.length; i2++) {
                itemsRef?.current[i2]?.classList.remove(styles.item__active)
                indicatorsRef?.current[i2]?.classList.remove(
                  styles.indicators_item__active,
                )
              }
              itemsRef.current[i].classList.add(styles.item__active)
              indicatorsRef.current[i].classList.add(
                styles.indicators_item__active,
              )
            }
            break
          }
        }
      }}
    >
      {images.map((item, index) => (
        <div
          className={styles.item}
          key={index}
          ref={(ref): void => {
            itemsRef.current[index] = ref
          }}
        >
          {(index === 0 || userInteracted) && (
            <UploadcareImage
              alt={alt}
              id={item?.uploadcareId}
              itemProp={itemProp}
              loading={index === 0 ? 'lazy' : 'eager'}
              size={{
                width: 500,
                height: 500,
              }}
            />
          )}
        </div>
      ))}
      <div className={styles.indicators}>
        {images.map((item, index) => (
          <span
            className={styles.indicators_item}
            key={index}
            ref={(ref): void => {
              indicatorsRef.current[index] = ref
            }}
          />
        ))}
      </div>
    </div>
  )
}

export default HoverSlider
