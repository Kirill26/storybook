import React from 'react'

interface Size {
  width: number
  height: number
}

interface B2BImageNewProps {
  alt?: string
  className?: string
  format?: 'auto' | 'webp' | 'png' | 'jpeg'
  id: string
  isDeleted?: boolean
  loading?: 'eager' | 'lazy' | undefined
  plug?: string
  preload?: boolean
  // https://uploadcare.com/docs/transformations/image/compression/
  quality?:
    | 'smart'
    | 'smart_retina'
    | 'normal'
    | 'better'
    | 'best'
    | 'lighter'
    | 'lightest'
  size: Size
  type?: 'smart_resize' | 'preview'
  getUrl?: boolean
  itemProp?: string
}

export const imgHost = 'https://img.b2b.trade'

export const getUploadcareImageUrl = (id = ''): string | null =>
  id ? `${imgHost}/${id}/-/preview/-/format/webp/` : null

export const UploadcareImage = ({
  alt = '',
  className = '',
  format = 'webp',
  id = '',
  isDeleted = false,
  loading,
  preload = false,
  quality = 'smart',
  size,
  type = 'smart_resize',
  plug,
  itemProp,
}: B2BImageNewProps): JSX.Element => {
  const url = id
    ? `${imgHost}/${id}/-/${type}/${size.width}x${size.height}/-/quality/${quality}/-/format/${format}/`
    : plug

  return (
    <>
      <img
        alt={alt}
        className={className}
        itemProp={itemProp}
        loading={loading}
        src={url}
      />
    </>
  )
}
