import React, { ReactElement, ReactNode } from 'react'
import ReactDOM from 'react-dom'

// eslint-disable-next-line import/prefer-default-export
export const Portal = (props: {
  children: ReactNode
  className?: string
}): ReactElement => {
  const el = React.useMemo(() => document.createElement('div'), [])

  React.useEffect(() => {
    const target = document.body
    const classList = ['portal']

    if (props.className)
      props.className.split(' ').forEach((item: string) => classList.push(item))
    classList.forEach((item) => el.classList.add(item))
    target.appendChild(el)

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    return () => {
      target.removeChild(el)
    }
  }, [el, props.className])

  return ReactDOM.createPortal(props.children, el)
}
