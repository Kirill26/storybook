import { useEffect } from 'react'

export const useClickOutside = (
  ref: any,
  callback: (arg0: any) => void,
): void => {
  useEffect(() => {
    function handleClickOutside(event: { target: any }): void {
      if (ref.current && !ref.current.contains(event.target)) {
        callback(event)
      }
    }

    document.addEventListener('mousedown', handleClickOutside)

    return (): void => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [ref])
}
